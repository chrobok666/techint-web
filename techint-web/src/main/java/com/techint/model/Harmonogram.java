package com.techint.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tech_harmongram", catalog = "m1608_techint")
public class Harmonogram {

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_HARM", unique = true, nullable = false)
	private int idHarmonogram;

	
	@Column(name = "USER_NAME", length = 45)
	private String username;

	@Column(name = "FILE_NAME", length = 45)
	private String fileName;
	
	@Column(name = "GRUPA", length = 45)
	private String grupa;
 
	public int getIdHarmonogram() {
		return idHarmonogram;
	}

	public void setIdHarmonogram(int idHarmonogram) {
		this.idHarmonogram = idHarmonogram;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getGrupa() {
		return grupa;
	}

	public void setGrupa(String grupa) {
		this.grupa = grupa;
	}
	
	
}