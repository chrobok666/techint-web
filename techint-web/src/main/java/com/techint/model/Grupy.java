package com.techint.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tech_grupy", catalog = "m1608_techint")
public class Grupy {
	
	@Id
	@Column(name = "NAZWA", unique = true, nullable = false, length = 45)
	private String nazwa;
	
	@Column(name = "USERNAME")
	private String username;
	
	@Column(name = "\"FROM\"")
	private int from;
	
	@Column(name = "\"TO\"")
	private int to;
	
	@Column(name = "TIME")
	private int time;
	
	@Column(name = "DAY")
	private int day;
	
	@Column(name = "ACTIVE")
	private boolean active;
	
 
	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	
	
	

}
