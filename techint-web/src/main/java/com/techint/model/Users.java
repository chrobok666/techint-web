package com.techint.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tech_users", catalog = "m1608_techint")
public class Users {

	
	@Id
	@Column(name = "NAME", unique = true, nullable = false, length = 45)
	private String username;

	@Column(name = "PASSWORD", nullable = false, length = 60)
	private String password;

	@Column(name = "EMAIL", nullable = false, length = 32)
	private String emailadress;
	
	@Column(name = "ACTIVE", nullable = false)
	private boolean active;
	
	@Column(name = "DEVICEID", nullable = false)
	private String deviceid;
	
	@Column(name = "SYNCH", nullable = false)
	private boolean isSynch;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>(0);


	public boolean isSynch() {
		return isSynch;
	}

	public void setSynch(boolean isSynch) {
		this.isSynch = isSynch;
	}

	public String getUsername() {
		return username;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailadress() {
		return emailadress;
	}

	public void setEmailadress(String emailadress) {
		this.emailadress = emailadress;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
}
