package com.techint.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tech_file", catalog = "m1608_techint")
public class File {

	@Id
	@Column(name = "ID_FILE", unique = true, nullable = false)
	private int idFile;

	@Column(name = "NAME", nullable = false, length = 60)
	private String name;

	@Column(name = "TYPE", nullable = false, length = 10)
	private String type;

	public int getIdFile() {
		return idFile;
	}

	public void setIdFile(int idFile) {
		this.idFile = idFile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
