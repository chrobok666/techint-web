package com.techint.controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.techint.model.Grupy;
import com.techint.model.Harmonogram;
import com.techint.model.Users;
import com.techint.service.impl.GrupyServiceImpl;
import com.techint.service.impl.HarmonogramService;
import com.techint.service.impl.LoginServiceImpl;
import com.techint.service.impl.UsersService;
import com.techint.utils.EchoThread;

@Controller
public class HomeController {

	static int port;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	private HarmonogramService harmonogramService;
	private UsersService usersService;

	@Autowired(required = true)
	@Qualifier(value = "harmonogramService")
	public void setHarmonogramService(HarmonogramService harmonogram) {
		this.harmonogramService = harmonogram;
	}

	@Autowired(required = true)
	@Qualifier(value = "usersService")
	public void setUsersService(UsersService users) {
		this.usersService = users;
	}

	private LoginServiceImpl loginService;

	@Autowired(required = true)
	@Qualifier(value = "loginService")
	public void setPersonService(LoginServiceImpl ps) {
		this.loginService = ps;
	}

	private GrupyServiceImpl grupyService;

	@Autowired(required = true)
	@Qualifier(value = "grupyService")
	public void setGrupyService(GrupyServiceImpl grupy) {
		this.grupyService = grupy;
	}

	@RequestMapping(value = { "/", "/home" })
	public String welcome() {

		return "home";
	}

	@RequestMapping("/register")
	public String register(Model model) {

		model.addAttribute("users", new Users());

		return "register";
	}

	@RequestMapping("/connect/{id}/{port}")
	public ResponseEntity<String> connect(@PathVariable("id") String id, @PathVariable("port") int port,
			RequestEntity<String> requestEntity, HttpServletRequest request) {

		setPort(port);

		String filePath = request.getServletContext().getRealPath("//uploads//");

		if (loginService.isexist(id)) {

			Users user = loginService.findByName(id);
			usersService.setActive(id);
			if (user.isSynch()) {
				System.out.println("true");
			}

			List<Harmonogram> harmo = harmonogramService.getAllHarmonogramForGrupa(id);
			List<Grupy> grupaUser = grupyService.getForUser(id);

			try {
				Path file = Paths.get(filePath + id + "_har.txt");

				String grupa, filename;

				Iterator itr = harmo.iterator();
				List<String> lines = new ArrayList<String>();
				while (itr.hasNext()) {
					Object[] obj = (Object[]) itr.next();
					grupa = String.valueOf(obj[3]);
					filename = String.valueOf(obj[2]);

					Grupy grupaTemp = grupyService.getGrupaById(grupa);

					lines.add(filename + "\t" + grupaTemp.getFrom() + "\t" + grupaTemp.getTo() + "\t"
							+ grupaTemp.getTime() + "\t" + grupaTemp.getDay());
				}
				Files.write(file, lines, Charset.forName("UTF-8"));

			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			Users newUser = new Users();
			newUser.setActive(true);
			newUser.setEmailadress(" ");
			newUser.setPassword("123");
			newUser.setUsername(id);
			newUser.setDeviceid(id);
			loginService.addUser(newUser);

			Users user = loginService.findByName(id);
			usersService.setActive(id);

			List<Harmonogram> harmo = harmonogramService.getAllHarmonogramForGrupa(id);
			List<Grupy> grupaUser = grupyService.getForUser(id);

			try {
				Path file = Paths.get(filePath + id + "_har.txt");

				String grupa, filename;

				Iterator itr = harmo.iterator();
				List<String> lines = new ArrayList<String>();
				while (itr.hasNext()) {
					Object[] obj = (Object[]) itr.next();
					grupa = String.valueOf(obj[3]);
					filename = String.valueOf(obj[2]);

					Grupy grupaTemp = grupyService.getGrupaById(grupa);
					lines.add(filename + "\t" + grupaTemp.getFrom() + "\t" + grupaTemp.getTo() + "\t"
							+ grupaTemp.getTime() + "\t" + grupaTemp.getDay());
				}

				Files.write(file, lines, Charset.forName("UTF-8"));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		ResponseEntity<String> responseEntity = new ResponseEntity<String>("connect ok", HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping("/endcon/{id}")
	public String endcon(@PathVariable("id") String id) {
		usersService.setActive(id);
		return "redirect:/home";
	}

	// private static Socket socket;
	private static String returnMessage;

	public static String getReturnMessage() {
		return returnMessage;
	}

	public static void setReturnMessage(String returnMessage) {
		HomeController.returnMessage = returnMessage;
	}

	private Socket socket;
	private static ServerSocket serverSocket;

	List<EchoThread> echoList = new ArrayList<EchoThread>();

	@RequestMapping("/socket")
	public String socket2() throws IOException {

		serverSocket = new ServerSocket(61230);
		serverSocket.setReuseAddress(true);
		System.out.println("Server Started and listening to the port 61230");
		while (true) {
			socket = serverSocket.accept();
			socket.setReuseAddress(true);

			EchoThread echo = new EchoThread(socket);

			echoList.add(echo);
			echo.start();

			System.out.println(echo.getName() + " " + echo.getId());
		}
	}

	@RequestMapping("/send")
	public String send() throws IOException {

		setReturnMessage("change");

		for (int i = 0; i < echoList.size(); i++) {

			echoList.get(i).sendMessage();
		}

		return "redirect:/home";
	}

	@RequestMapping("/close")
	public String socketclose() throws IOException {

		socket.close();
		serverSocket.close();

		return "redirect:/home";
	}

}