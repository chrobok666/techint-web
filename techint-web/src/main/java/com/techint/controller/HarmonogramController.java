package com.techint.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.techint.model.Grupy;
import com.techint.model.Harmonogram;
import com.techint.service.impl.FileServiceImpl;
import com.techint.service.impl.GrupyServiceImpl;
import com.techint.service.impl.HarmonogramService;
import com.techint.service.impl.UsersService;

@Controller
public class HarmonogramController {

	private HarmonogramService harmonogramService;
	private UsersService usersService;
	private GrupyServiceImpl grupyService;

	@Autowired(required = true)
	@Qualifier(value = "harmonogramService")
	public void setHarmonogramService(HarmonogramService harmonogram) {
		this.harmonogramService = harmonogram;
	}

	@Autowired(required = true)
	@Qualifier(value = "grupyService")
	public void setGrupyService(GrupyServiceImpl grupy) {
		this.grupyService = grupy;
	}

	@Autowired(required = true)
	@Qualifier(value = "usersService")
	public void setUsersService(UsersService users) {
		this.usersService = users;
	}

	FileServiceImpl fileService;

	@Autowired(required = true)
	@Qualifier(value = "fileService")
	public void setHarmonogramService(FileServiceImpl file) {
		this.fileService = file;
	}


	@RequestMapping("/harmonogram")
	public String harmonogram(Model model) {
		model.addAttribute("harmongram", harmonogramService.getAllHarmonogram());
		model.addAttribute("grupy", grupyService.getAllGrupy());
		return "harmonogram";
	}

	@RequestMapping(value = "harmonogram/addHarmonogram")
	public String addItem(@ModelAttribute("harmongram") Harmonogram harmongram) {
		harmonogramService.addHarmonogram(harmongram);
		usersService.setSynch(harmongram.getUsername());
		return "redirect:/harmonogram";
	}

	@RequestMapping("harmonogram/addHarm")
	public ModelAndView harmonogramAdd(Model model) {
		model.addAttribute("users", usersService.getAllUsers());
		model.addAttribute("plik", fileService.getAllfiles());
		model.addAttribute("grupy", grupyService.getAllGrupy());

		return new ModelAndView("harmonogramDodaj", "harmongram", new Harmonogram());
	}

	@RequestMapping("harmonogram/addGrupa")
	public ModelAndView grupaAdd(Model model) {
		model.addAttribute("users", usersService.getAllUsers());

		return new ModelAndView("grupaDodaj", "grupy", new Grupy());
	}

	@RequestMapping(value = "harmonogram/addGrupy")
	public String addGrupy(@ModelAttribute("grupy") Grupy grupy) {
		grupyService.addGrupa(grupy);
		return "redirect:/harmonogram";
	}

	@RequestMapping("harmonogram/delharmonogram/{id}")
	public String deleteItem(@PathVariable("id") Integer id) {
		harmonogramService.deleteHarmonogram(id);
		return "redirect:/harmonogram";
	}

	@RequestMapping("harmonogram/editharmonogram/{id}")
	public ModelAndView editItemId(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("users", usersService.getAllUsers());
		model.addAttribute("plik", fileService.getAllfiles());
		model.addAttribute("grupy", grupyService.getAllGrupy());
		return new ModelAndView("editHarmonogram", "harmongram", harmonogramService.getHarmonogramById(id));
	}


	@RequestMapping(value = "harmonogram/editharmonogram/edit")
	public String editHarmonogram(@ModelAttribute("harmongram") Harmonogram harmonogram) {
		harmonogramService.updateHarmonogram(harmonogram);
		return "redirect:/harmonogram";
	}

	@RequestMapping(value = "harmonogram/editgrupa/edit")
	public String editGrupa(@ModelAttribute("grupy") Grupy grupy) {
		grupyService.updateGrupy(grupy);
		return "redirect:/harmonogram";
	}

	@RequestMapping(value = "harmonogram/delgrupa/{name}")
	public String delGrupa(@PathVariable("name") String name) {
		grupyService.deleteGrupa(name);
		return "redirect:/harmonogram";
	}

	@RequestMapping("harmonogram/editgrupa/{name}")
	public ModelAndView editGrupyId(@PathVariable("name") String name, Model model) {
		model.addAttribute("users", usersService.getAllUsers());
		model.addAttribute("plik", fileService.getAllfiles());
		return new ModelAndView("editgrupy", "grupy", grupyService.getGrupaById(name));
	}

	@RequestMapping("harmonogram/activegrupa/{name}")
	public String activeItem(@PathVariable("name") String name) {

		Grupy grupa = grupyService.getGrupaById(name);

		if (grupa.isActive())
			grupa.setActive(false);
		else
			grupa.setActive(true);

		grupyService.updateGrupy(grupa);
		usersService.setSynch(name);
		return "redirect:/harmonogram";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

}