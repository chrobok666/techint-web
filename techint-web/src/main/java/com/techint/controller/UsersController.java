package com.techint.controller;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.techint.model.Grupy;
import com.techint.model.Harmonogram;
import com.techint.service.impl.GrupyServiceImpl;
import com.techint.service.impl.HarmonogramService;
import com.techint.service.impl.UsersService;

@Controller
public class UsersController {

	private static Socket socket;

	private HarmonogramService harmonogramService;
	private UsersService usersService;

	@Autowired(required = true)
	@Qualifier(value = "harmonogramService")
	public void setHarmonogramService(HarmonogramService harmonogram) {
		this.harmonogramService = harmonogram;
	}

	@Autowired(required = true)
	@Qualifier(value = "usersService")
	public void setUsersService(UsersService users) {
		this.usersService = users;
	}

	private GrupyServiceImpl grupyService;

	@Autowired(required = true)
	@Qualifier(value = "grupyService")
	public void setGrupyService(GrupyServiceImpl grupy) {
		this.grupyService = grupy;
	}

	@RequestMapping("/uzytkownicy")
	public String uzytkownicy(Model model) {
		model.addAttribute("users", usersService.getAllUsers());
		return "uzytkownicy";
	}

	@RequestMapping("uzytkownicy/synch/{name}")
	public String synchItem(@PathVariable("name") String user, HttpServletRequest request) {
		usersService.setSynch(user);

		String filePath = request.getServletContext().getRealPath("//uploads//");

		try {

			String host = "localhost";
			int port = HomeController.port;
			InetAddress address = InetAddress.getByName(host);
			socket = new Socket(address, port);
			socket.setReuseAddress(true);
			OutputStream os = socket.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			BufferedWriter bw = new BufferedWriter(osw);

			String number = "change";

			String sendMessage = number + "\n";
			bw.write(sendMessage);
			bw.flush();
			System.out.println("Message sent to the client : " + sendMessage);

			List<Harmonogram> harmo = harmonogramService.getAllHarmonogramForGrupa(user);
			List<Grupy> grupaUser = grupyService.getForUser(user);

			try {
				Path file = Paths.get(filePath + user + "_har.txt");

				String grupa, filename;

				Iterator itr = harmo.iterator();
				List<String> lines = new ArrayList<String>();
				while (itr.hasNext()) {
					Object[] obj = (Object[]) itr.next();
					grupa = String.valueOf(obj[3]);
					filename = String.valueOf(obj[2]);

					Grupy grupaTemp = grupyService.getGrupaById(grupa);

					lines.add(filename + "\t" + grupaTemp.getFrom() + "\t" + grupaTemp.getTo() + "\t"
							+ grupaTemp.getTime() + "\t" + grupaTemp.getDay());
				}
				Files.write(file, lines, Charset.forName("UTF-8"));
				socket.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return "redirect:/uzytkownicy";
	}

	@RequestMapping(value = "uzytkownicy/getfile/{file_name:.+}", method = RequestMethod.GET)
	public void getFile(@PathVariable("file_name") String fileName, HttpServletResponse response,
			HttpServletRequest request) {
		try {
			String filePath = request.getServletContext().getRealPath("//uploads//");
			InputStream is = new FileInputStream(filePath + fileName);
			org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			System.out.println("Error writing file to output stream. Filename was '" + fileName);
		}
	}
}