package com.techint.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import com.techint.model.FileUpload;
import com.techint.service.impl.FileServiceImpl;

@Controller
public class BibliotekaController {

	FileServiceImpl fileService;

	@Autowired(required = true)
	@Qualifier(value = "fileService")
	public void setHarmonogramService(FileServiceImpl file) {
		this.fileService = file;
	}

	@RequestMapping("/biblioteka")
	public ModelAndView biblioteka(Model model) {
		model.addAttribute("plik", fileService.getAllfiles());
		return new ModelAndView("biblioteka", "file", new FileUpload());
	}

	@RequestMapping(value = "biblioteka/addfile", method = RequestMethod.POST)
	public String fileUploaded(@RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {

		return "biblioteka";
	}

	@RequestMapping(value = "biblioteka/delfile/{id}")
	public String delGrupa(@PathVariable("id") int id, HttpServletRequest request) {
		fileService.delFile(id);
		return "redirect:/biblioteka";
	}
}