package com.techint.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.techint.model.File;
import com.techint.service.impl.FileServiceImpl;

@MultipartConfig
@WebServlet("/uploadServlet")
public class UploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	FileServiceImpl fileService;

	@Autowired(required = true)
	@Qualifier(value = "fileService")
	public void setHarmonogramService(FileServiceImpl file) {
		this.fileService = file;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Part file = request.getPart("file");
		String filename = getFilename(file);

		String filePath = request.getServletContext().getRealPath("//uploads//");
		System.out.println(filePath);

		InputStream filecontent = file.getInputStream();

		filename.substring(filename.length() - 4, filename.length());
		FileOutputStream fos = new FileOutputStream(new java.io.File(filePath + "\\" + filename));
		int inByte;
		while ((inByte = filecontent.read()) != -1) {
			fos.write(inByte);
		}
		filecontent.close();
		fos.close();

		File plik = new File();
		plik.setName(filename);
		plik.setType("tst");
		fileService.addFiles(plik);

		response.sendRedirect("biblioteka");

	}

	private static String getFilename(Part part) {
		for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().startsWith("filename")) {
				String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE
																													// fix.
			}
		}
		return null;
	}

}
