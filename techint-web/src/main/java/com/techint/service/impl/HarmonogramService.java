package com.techint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.techint.dao.HarmonogramDao;
import com.techint.model.Harmonogram;

@Service("harmonogramService")
public class HarmonogramService {
	
	@Autowired
	HarmonogramDao harmonogramDao;
	
	public List<Harmonogram> getAllHarmonogram(){	
		return harmonogramDao.getAllHarmonogram();
	}
	
	public List<Harmonogram> getAllHarmonogramForGrupa(String username){	
		return harmonogramDao.getAllHarmonogramForGrupa(username);
	}
	
	@Transactional
	public void deleteHarmonogram(int id) {
		harmonogramDao.deleteHarmonogram(id);	
	}
	
	@Transactional
	public Harmonogram getHarmonogramById(int id) {
		return harmonogramDao.getHarmonogramById(id);
	}
	
	@Transactional
	public void updateHarmonogram(Harmonogram harmonogram){
		harmonogramDao.updateHarmonogram(harmonogram);
	}
	
	@Transactional
	public void addHarmonogram(Harmonogram harmonogram) {
		harmonogramDao.addHarmonogramy(harmonogram);
	}
	
	@Transactional
	public List<Harmonogram> getForGrupa(String grupa){	
		return harmonogramDao.getForGrup(grupa);
	}
}
