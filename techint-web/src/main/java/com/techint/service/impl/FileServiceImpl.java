package com.techint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.FileDao;
import com.techint.model.File;

@Service("fileService")
public class FileServiceImpl {	
	
	@Autowired
	FileDao fileDao;
	
	public List<File> getAllfiles(){	
		return fileDao.getAllFile();
	}
	
	@Transactional
	public int addFiles(File file) {
		return fileDao.addFile(file);
	}
	
	@Transactional
	public void delFile(int id){
		fileDao.deleteFile(id);
	}
}