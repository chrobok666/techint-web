package com.techint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.GrupyDao;
import com.techint.model.Grupy;

@Service("grupyService")
public class GrupyServiceImpl {
	
	@Autowired
	GrupyDao grupyDao;
	
	public List<Grupy> getAllGrupy(){
		return grupyDao.getAllGrupy();
	}
	
	@Transactional
	public void addGrupa(Grupy grupa){
		grupyDao.addGrupy(grupa);
	}
	
	public Grupy getGrupaById(String nazwa) {
		return grupyDao.getGrupaById(nazwa);
	}
	
	public void updateGrupy(Grupy grupa){
		grupyDao.updateGrupa(grupa);
	}

	public List<Grupy> getForUser(String username){
		return grupyDao.getGrupaByUser(username);
	}
	
	public void deleteGrupa(String name){
		grupyDao.deleteGrupa(name);
	}
}
