package com.techint.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.LoginDao;
import com.techint.model.UserRole;
import com.techint.model.Users;



@Service("loginService")
public class LoginServiceImpl implements UserDetailsService {

	@Autowired
	LoginDao loginDao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		Users user = loginDao.findByUserName(username);

		List<GrantedAuthority> authorities = buildUserAuthority(user
				.getUserRole());

		return buildUserForAuthentication(user, authorities);
	}

	private User buildUserForAuthentication(Users user,
			List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(),
				user.isActive(), true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(
				setAuths);

		return Result;
	}
	
	@Transactional
	public void addUser(Users user){

		UserRole userRole = new UserRole();
		
		userRole.setRole("ROLE_USER");
		userRole.setUser(user);
		
		
		 loginDao.addUser(user);
		 
		 loginDao.addUserRole(userRole);
	}
	
	@Transactional
	public void updateUser(Users user){
		
		loginDao.updateUser(user);
		
		
	}
	
	public Users findByName(String name){
		return loginDao.findByUserName(name);
	}
	
	public boolean isexist(String username){
		
		return loginDao.isExist(username);
	}

}
