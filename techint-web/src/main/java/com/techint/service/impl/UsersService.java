package com.techint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.UsersDao;
import com.techint.model.Users;

@Service("usersService")
public class UsersService {

	@Autowired
	UsersDao usersDao;

	@Transactional
	public void setSynch(String name) {
		usersDao.setSynch(name);
	}

	public List<Users> getAllUsers() {
		return usersDao.getAllUsers();
	}

	public Users getUserByName(String name) {
		return usersDao.getUserByName(name);
	}

	@Transactional
	public void setActive(String username) {

		usersDao.setActive(username);
	}
}