package com.techint.dao;



import com.techint.model.UserRole;
import com.techint.model.Users;

public interface LoginDao {
	Users findByUserName(String username);
	
	void addUser(Users user);
	void updateUser(Users user);
	
	void addUserRole(UserRole userRole);
	
	boolean isExist(String username);
	
}
