package com.techint.dao;

import java.util.List;


import com.techint.model.Users;

public interface UsersDao {
	
	List<Users> getAllUsers();
	
	void setSynch(String name);
	
	Users getUserByName(String name);
	
	void setActive(String id);

}
