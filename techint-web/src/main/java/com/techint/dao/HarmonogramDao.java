package com.techint.dao;

import java.util.List;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import com.techint.model.Harmonogram;

public interface HarmonogramDao {

	List<Harmonogram> getAllHarmonogram();
	
	List<Harmonogram> getAllHarmonogramForGrupa(String username);

	void deleteHarmonogram(int id);

	Harmonogram getHarmonogramById(int id);
	
	void updateHarmonogram(Harmonogram harmonogram);

	void addHarmonogramy(Harmonogram harmonogram);
	
	List<Harmonogram> getForGrup(String grupa);

}
