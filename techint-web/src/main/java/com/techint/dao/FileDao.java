package com.techint.dao;

import java.util.List;

import com.techint.model.File;

public interface FileDao {
	
	List<File> getAllFile();

	int addFile(File file);

	void deleteFile(int id);
	


}
