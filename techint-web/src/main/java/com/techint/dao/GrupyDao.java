package com.techint.dao;

import java.util.List;

import com.techint.model.Grupy;

public interface GrupyDao {
	
	void addGrupy(Grupy grupa);
	
	List<Grupy> getAllGrupy();
	
	void deleteGrupa(String nazwa);
	
	Grupy getGrupaById(String nazwa);
	
	void updateGrupa(Grupy grupa);
	
	List<Grupy> getGrupaByUser(String username);
}
