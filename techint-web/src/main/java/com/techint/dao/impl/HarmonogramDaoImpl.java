package com.techint.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.techint.dao.HarmonogramDao;
import com.techint.model.Harmonogram;

public class HarmonogramDaoImpl implements HarmonogramDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	org.hibernate.Transaction tx = null;

	@Override
	@Transactional
	public void addHarmonogramy(Harmonogram harmonogram) {

		Session session = sessionFactory.openSession();
		Transaction tx = (Transaction) session.beginTransaction();
		session.saveOrUpdate(harmonogram);
		tx.commit();
		session.close();
	}

	@Override
	public List<Harmonogram> getAllHarmonogram() {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Harmonogram> list = session.createSQLQuery("select * from tech_harmongram").list();
		return list;
	}

	@Override
	@Transactional
	public void deleteHarmonogram(int id) {
		Session session = sessionFactory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();

		Harmonogram harmonogram = (Harmonogram) session.load(Harmonogram.class, id);
		session.delete(harmonogram);
		tx.commit();
		session.close();
	}

	@Override
	public Harmonogram getHarmonogramById(int id) {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();

		Harmonogram harmonogram = (Harmonogram) session.load(Harmonogram.class, id);
		tx.commit();
		return harmonogram;
	}

	@Override
	@Transactional
	public void updateHarmonogram(Harmonogram harmonogram) {
		Session session = sessionFactory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		session.saveOrUpdate(harmonogram);
		tx.commit();
		session.close();
	}

	@Override
	public List<Harmonogram> getForGrup(String grupa) {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Harmonogram> list = session.createSQLQuery("select * from tech_harmongram where GRUPA='" + grupa + "'")
				.list();
		return list;
	}

	@Override
	public List<Harmonogram> getAllHarmonogramForGrupa(String username) {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Harmonogram> list = session.createSQLQuery(
				"select * from tech_harmongram where grupa in (select nazwa from tech_grupy where username='" + username
						+ "' and active=1)")
				.list();
		return list;
	}
}
