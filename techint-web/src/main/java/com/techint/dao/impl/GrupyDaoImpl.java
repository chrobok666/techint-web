package com.techint.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.GrupyDao;
import com.techint.model.Grupy;

public class GrupyDaoImpl implements GrupyDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	org.hibernate.Transaction tx = null;

	@Transactional
	@Override
	public void addGrupy(Grupy grupa) {
		Session session = sessionFactory.openSession();
		Transaction tx = (Transaction) session.beginTransaction();
		session.saveOrUpdate(grupa);
		tx.commit();
		session.close();

	}

	@Transactional
	@Override
	public List<Grupy> getAllGrupy() {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Grupy> list = session.createSQLQuery("select * from tech_grupy").list();
		return list;
	}

	@Override
	public void deleteGrupa(String nazwa) {

		Session session = sessionFactory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();

		Grupy grupy = (Grupy) session.load(Grupy.class, nazwa);
		session.delete(grupy);
		tx.commit();
		session.close();

	}

	@Transactional
	@Override
	public Grupy getGrupaById(String nazwa) {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();

		Grupy grupa = (Grupy) session.load(Grupy.class, nazwa);
		tx.commit();
		session.close();
		return grupa;
	}

	@Transactional
	@Override
	public void updateGrupa(Grupy grupa) {
		Session session = sessionFactory.openSession();
		Transaction tx = (Transaction) session.beginTransaction();
		session.saveOrUpdate(grupa);
		tx.commit();
		session.close();

	}

	@Transactional
	@Override
	public List<Grupy> getGrupaByUser(String username) {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Grupy> list = session
				.createSQLQuery("select * from tech_grupy where username='" + username + "' and active=1").list();
		return list;
	}

}
