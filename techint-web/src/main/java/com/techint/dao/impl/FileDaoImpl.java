package com.techint.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.FileDao;
import com.techint.model.File;

public class FileDaoImpl implements FileDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	org.hibernate.Transaction tx = null;

	@Override
	@Transactional
	public List<File> getAllFile() {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<File> list = session.createSQLQuery("select * from tech_file").list();
		return list;
	}

	@Override
	@Transactional
	public int addFile(File file) {
		Session session = sessionFactory.openSession();
		Transaction tx = (Transaction) session.beginTransaction();
		session.saveOrUpdate(file);
		tx.commit();
		session.close();
		return file.getIdFile();
	}

	@Override
	public void deleteFile(int id) {

		Session session = sessionFactory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();

		File file = (File) session.load(File.class, id);
		session.delete(file);
		tx.commit();
		session.close();
	}
}