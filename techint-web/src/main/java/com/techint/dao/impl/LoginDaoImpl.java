package com.techint.dao.impl;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.techint.dao.LoginDao;
import com.techint.model.UserRole;
import com.techint.model.Users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Repository("loginDao")
public class LoginDaoImpl implements LoginDao{
	
	private static final Logger logger = LoggerFactory.getLogger(LoginDaoImpl.class);
	
	@Autowired   
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;

	@Override
	@Transactional
	public Users findByUserName(String username) {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		Users user = (Users) session.load(Users.class, new String(username));
		tx.commit();
		session.close();
		return user;
	}
	@Override
	@Transactional
	public void addUser(Users user) {
        Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(user);
		tx.commit();
		Serializable id = session.getIdentifier(user);
		session.close();

	}

	@Override
	public void updateUser(Users user) {
		Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        logger.info("Person updated successfully, Person Details="+user);
        //session.close();
	}

	@Override
	@Transactional
	public void addUserRole(UserRole userRole) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(userRole);
		tx.commit();
		Serializable id = session.getIdentifier(userRole);
		session.close();
	}
	
	
	@Override
	public boolean isExist(String username) {
		
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		boolean is = false;
	
		try{
			session.load(Users.class, new String(username)).toString();
			is=true;
		}catch(Exception e){
			is=false;
		}
        
	    session.getTransaction().commit();
	    session.close();
	    
	    return is;
	}
}