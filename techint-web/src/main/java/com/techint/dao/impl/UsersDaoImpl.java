package com.techint.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.techint.dao.UsersDao;
import com.techint.model.Harmonogram;
import com.techint.model.Users;

public class UsersDaoImpl implements UsersDao {
	
	private static final Logger logger = LoggerFactory.getLogger(HarmonogramDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	Session session = null;
	org.hibernate.Transaction tx = null;
	
	@Override
	public void setSynch(String name) {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		
		session.createSQLQuery("update tech_users set synch = not synch where name='" + name + "'").executeUpdate(); 
		
		tx.commit();
		session.close();
	}

	@Override
	public List<Users> getAllUsers() {
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		session.beginTransaction();
		List<Users> list = session.createSQLQuery("select * from tech_users").list();
		return list;
	}

	@Override
	public Users getUserByName(String username) {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		
		Users user= (Users) session.load(Users.class, username);
		tx.commit();
		return user;
	}
	
	@Override
	@Transactional
	public void setActive(String id) { 
		session = sessionFactory.openSession();
		tx = session.getTransaction();
		
		
		session.createSQLQuery("update tech_users set active = not active where name='" + id + "'").executeUpdate();
		session.beginTransaction(); 

		tx.commit();
		session.close();
	}

}
