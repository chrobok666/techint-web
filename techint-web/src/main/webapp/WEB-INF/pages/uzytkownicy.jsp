<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">


<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<script src="http://bootsnipp.com/dist/scripts.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066">

<link href="<c:url value="/css/style.css" />" rel="stylesheet">

<script src="<c:url value="/js/dropzone.js" />"></script>

<title>Biblioteka multimediow</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/navheader.jsp" />

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<jsp:include page="/WEB-INF/pages/menu.jsp" />
		</div>


		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>Nazwa</th>
							<th>Email</th>
							<th>Aktywny</th>
							<th>Sesja id</th>
							<th>Zsynchronizowany</th>
							<th>Synchronizuj</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="list" items="${users}">
							<tr>
								<td>${list[0]}</td>
								<td>${list[2]}</td>
								<td>${list[3]}</td>
								<td>${list[4]}</td>
								<td>${list[5]}</td>
								<!--  <td><a href="uzytkownicy/synch/${list[0]}"
									class="btn btn-danger " role="button"> S </a></td>-->
									<td><a href="send"
									class="btn btn-danger " role="button"> S </a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>


			</div>
		</div>
	</div>


</body>
</html>