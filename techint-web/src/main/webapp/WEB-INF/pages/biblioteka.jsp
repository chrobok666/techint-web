<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">


<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<script src="http://bootsnipp.com/dist/scripts.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066">


<link href="<c:url value="/css/style.css" />" rel="stylesheet">

<script src="<c:url value="/js/dropzone.js" />"></script>

<title>Biblioteka multimediow</title>




</head>
<body>

	<jsp:include page="/WEB-INF/pages/navheader.jsp" />

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<jsp:include page="/WEB-INF/pages/menu.jsp" />
		</div>


		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div>
					<!-- /techint-web/biblioteka/addfile?${_csrf.parameterName}=${_csrf.token} -->
					<form method="POST"
						action="uploadServlet?${_csrf.parameterName}=${_csrf.token}"
						enctype="multipart/form-data">

						Dodaj plik: <input type="file" name="file" />
						<button type="submit" value="upload" class="btn btn-default">Dodaj</button>
					</form>

				</div>

				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>NAZWA</th>
							<th>TYP</th>
							<th>USUŃ</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="list" items="${plik}">
							<tr>
								<td>${list[0]}</td>
								<td>${list[1]}</td>
								<td>${list[2]}</td>
								<td><a href="biblioteka/delfile/${list[0]}"
									class="btn btn-danger " role="button"> X </a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

			</div>
		</div>


	</div>


</body>
</html>