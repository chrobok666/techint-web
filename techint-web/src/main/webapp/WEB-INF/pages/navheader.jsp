<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<nav class="navbar navbar-inverse  navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="/techint-web/"><span
				class="glyphicon glyphicon-folder-open"></span> TechIntPK</a>
		</div>
		<ul class="nav navbar-nav  navbar-right">
			<li><a href="/techint-web/register"><span
					class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
			<li><a href="/techint-web/login"><span
					class="glyphicon glyphicon-log-in"></span> Zaloguj</a></li>
		</ul>
	</div>
</nav>
