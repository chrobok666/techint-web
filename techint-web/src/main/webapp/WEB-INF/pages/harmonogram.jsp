<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous" />


<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>

<script src="http://bootsnipp.com/dist/scripts.min.js"></script>

<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" />

<link rel="stylesheet"
	href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066" />

<link href="<c:url value="/css/style.css" />" rel="stylesheet" />


<spring:url value="/js/jquery.js" var="jqueryDatepicker" />
<spring:url value="/css/datepicker.css" var="datepicker" />
<spring:url value="/js/bootstrap-datepicker.js"
	var="bootstrapDatepicker" />

<link href="${datepicker}" rel="stylesheet" />
<script type="text/javascript" src="${bootstrapDatepicker}"></script>

<title>Harmonogram</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/navheader.jsp" />

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<jsp:include page="/WEB-INF/pages/menu.jsp" />
		</div>


		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<p>Listy odtwarzania</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>NAZWA</th>
							<th>Przypisany użytkownik</th>
							<th>OD</th>
							<th>DO</th>
							<th>CZAS</th>
							<th>DZIEŃ</th>
							<th>AKTYWNY?</th>
							<th>Aktywuj / deaktywuj</th>
							<th>EDYTUJ</th>
							<th>USUŃ</th>


						</tr>
					</thead>
					<tbody>
						<c:forEach var="list" items="${grupy}">
							<tr>
								<td>${list[0]}</td>
								<td>${list[1]}</td>
								<td>${list[2]}</td>
								<td>${list[3]}</td>
								<td>${list[4]}</td>
								<td>${list[5]}</td>
								<td>${list[6]}</td>
								<td><a href="harmonogram/activegrupa/${list[0]}"
									class="btn btn-info " role="button"> A </a></td>
								<td><a href="harmonogram/editgrupa/${list[0]}"
									class="btn btn-info " role="button"> E </a></td>
								<td><a href="harmonogram/delgrupa/${list[0]}"
									class="btn btn-danger " role="button"> X </a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br /> <br /> <a href="harmonogram/addGrupa" class="btn btn-info "
					role="button"> Dodaj nową liste </a> <br /> <br />

				<p>Przypisz plik do listy, użytkownika</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Przypisany użytkownik</th>
							<th>Przypisany plik</th>
							<th>Lista</th>
							<th>EDYTUJ</th>
							<th>USUŃ</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="list" items="${harmongram}">
							<tr>
								<td>${list[0]}</td>
								<td>${list[1]}</td>
								<td>${list[2]}</td>
								<td>${list[3]}</td>
								<td><a href="harmonogram/editharmonogram/${list[0]}"
									class="btn btn-info " role="button"> E </a></td>
								<td><a href="harmonogram/delharmonogram/${list[0]}"
									class="btn btn-danger " role="button"> X </a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>


				<br /> <br /> <a href="harmonogram/addHarm" class="btn btn-info "
					role="button"> Przypisz </a> <br /> <br /> <br /> <br /> <br />
				<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
				<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
				<br />

			</div>
		</div>
</body>
</html>


