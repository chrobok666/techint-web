<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
	integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
	crossorigin="anonymous">
-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<script src="http://bootsnipp.com/dist/scripts.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066">

<link href="<c:url value="/css/style.css" />" rel="stylesheet">


<spring:url value="/js/jquery.js" var="jqueryDatepicker" />
<spring:url value="/css/datepicker.css" var="datepicker" />
<spring:url value="/js/bootstrap-datepicker.js"
	var="bootstrapDatepicker" />

<link href="${datepicker}" rel="stylesheet" />
<script type="text/javascript" src="${bootstrapDatepicker}"></script>

<title>Harmonogram</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/navheader.jsp" />

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<jsp:include page="/WEB-INF/pages/menu.jsp" />
		</div>


		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<h2>Edytuj</h2>


				<form:form id="addForm" modelAttribute="grupy" method="post"
					action="edit" class="form-horizontal" role="form">


					<table class="table table-striped">
						<thead>
							<tr>
										<th><label>Użytkownik:</label> <form:select
										path="username" class="form-control" id="username">
										<c:forEach var="list" items="${users}">
											<option>${list[0]}</option>
										</c:forEach>
									</form:select></th>

								<th><div class="form-group">
										<label class="control-label col-sm-2" for="nazwa">Nazwa:</label>
										<div class="col-sm-10">
											<form:input path="nazwa" class="form-control" />
										</div>
									</div>
								</th>
								
								<th><div class="form-group">
										<label class="control-label col-sm-2" for="from">Od:</label>
										<div class="col-sm-10">
											<form:input path="from" class="form-control" />
										</div>
									</div>
								</th>
								
								<th><div class="form-group">
										<label class="control-label col-sm-2" for="to">Do:</label>
										<div class="col-sm-10">
											<form:input path="to" class="form-control" />
										</div>
									</div>
								</th>
								
								<th><div class="form-group">
										<label class="control-label col-sm-2" for="time">Czas:</label>
										<div class="col-sm-10">
											<form:input path="time" class="form-control" />
										</div>
									</div>
								</th>
								
								<th><div class="form-group">
										<label class="control-label col-sm-2" for="day">Dzień:</label>
										<div class="col-sm-10">
											<form:input path="day" class="form-control" />
										</div>
									</div>
								</th>
								
								<th><label>Active:</label> <form:select
										path="active" class="form-control" id="active">
										
											<option>1</option>
											<option>0</option>
									</form:select>
								</th>
							</tr>
						</thead>
					</table>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" value="edit" class="btn btn-default">Edytuj</button>
						</div>
					</div>



				</form:form>



			</div>
		</div>
</body>
</html>


