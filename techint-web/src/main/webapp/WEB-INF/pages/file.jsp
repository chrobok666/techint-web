<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<body>

	<h2>Spring MVC - Uploading a file..</h2>
	<form:form method="POST" modelAttribute="file" action="file/addfile"
		enctype="multipart/form-data">
 
		Upload your file please: 
		<form:input type="file" path="file" />
		<button type="submit" value="upload" class="btn btn-default">Dodaj</button>
	</form:form>

</body>
</html>