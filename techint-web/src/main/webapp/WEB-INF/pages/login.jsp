<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">


<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<script src="http://bootsnipp.com/dist/scripts.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066">

<link href="<c:url value="/css/style.css" />" rel="stylesheet">


<title>Login</title>
</head>
<body>

	<jsp:include page="/WEB-INF/pages/navheader.jsp" />

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<jsp:include page="/WEB-INF/pages/menu.jsp" />
		</div>


		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-th-list"></span> Logowanie
					</h3>
				</div>


				<div class="col-sm-6">
					<span style="color: red">${message}</span>
					<form:form method="post" action="j_spring_security_check"
						modelAttribute="users" class="form-horizontal" role="form">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Email:</label>
							<div class="col-sm-10">
								<form:input path="username" class="form-control" id="email"
									placeholder="Enter email" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="pwd">Password:</label>
							<div class="col-sm-10">
								<form:input path="password" type="password" class="form-control"
									id="pwd" placeholder="Enter password" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Zaloguj</button>
							</div>
						</div>
					</form:form>

				</div>




			</div>
		</div>
	</div>


</body>
</html>
